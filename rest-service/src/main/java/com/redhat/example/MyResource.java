package com.redhat.example;

import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/get-id")
public class MyResource {

    private static AtomicInteger atomicInt = new AtomicInteger(0);

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public int getId() {
        return atomicInt.incrementAndGet();
    }   
}