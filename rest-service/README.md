# rest-service project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Deploying in OpenShift

1. Create an _über-jar_:
   ```shell script
   mvn clean package -Dquarkus.package.type=uber-jar
   ```
   The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

2. In openshift create the build, build and create the app

   ```sh
   oc new-build registry.access.redhat.com/openjdk/openjdk-11-rhel7:latest --binary --name=rest-service -l app=rest-service
   oc start-build rest-service --from-file target/*-runner.jar --follow
   oc new-app rest-service
   ```