package org.acme;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.acme.config.KieServerConfig;
import org.kie.server.api.model.instance.TaskSummary;

@Path("/tasks")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ExampleResource {

    @Inject
    KieServerConfig config;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<TaskSummary> tasks() {
        // var list = new ArrayList<String>();
        // list.add("task 1");
        // list.add("task 2");
        // return list;
        KieClient client = new KieClient(config);
        return client.taskList();
    }
}