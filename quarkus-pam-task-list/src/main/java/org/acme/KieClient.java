package org.acme;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acme.config.KieServerConfig;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.instance.TaskSummary;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.UserTaskServicesClient;

public class KieClient {

    KieServerConfig config;

    public KieClient(KieServerConfig config){
        this.config = config;
    }

    public List<TaskSummary> taskList() {
        List<TaskSummary> list = null;
        try {
            KieServicesClient client = getClient();

            UserTaskServicesClient userTaskServicesClient = client.getServicesClient(UserTaskServicesClient.class);
            list = userTaskServicesClient.findTasksAssignedAsPotentialOwner(config.getUsername(), 0, 20);
            // for (TaskSummary taskSummary : list) {
            //     if (taskSummary.getSubject().isEmpty()) {
            //         result.add(taskSummary.getName());                    
            //     } else {
            //         result.add(taskSummary.getSubject());
            //     }
            // }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private KieServicesClient getClient() {
        KieServicesConfiguration restConfig = KieServicesFactory.newRestConfiguration(config.getUrl(), config.getUsername(), config.getPassword());

        // Configuration for JMS
        // KieServicesConfiguration config =
        // KieServicesFactory.newJMSConfiguration(connectionFactory, requestQueue,
        // responseQueue, username, password)

        // Marshalling
        restConfig.setMarshallingFormat(MarshallingFormat.JSON);
        Set<Class<?>> extraClasses = new HashSet<Class<?>>();
        // extraClasses.add(OrderInfo.class);
        // extraClasses.add(SupplierInfo.class);
        restConfig.addExtraClasses(extraClasses);
        Map<String, String> headers = null;
        restConfig.setHeaders(headers);
        KieServicesClient client = KieServicesFactory.newKieServicesClient(restConfig);

        return client;
    }

}
