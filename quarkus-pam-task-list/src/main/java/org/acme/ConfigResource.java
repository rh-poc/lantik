package org.acme;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.acme.config.KieServerConfig;

@Path("/config")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConfigResource {

    @Inject
    KieServerConfig config;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String,String> config() {
        var configMap = new HashMap<String,String>();
        configMap.put("url",config.getUrl());
        configMap.put("container",config.getContainer());
        return configMap;
    }
}
