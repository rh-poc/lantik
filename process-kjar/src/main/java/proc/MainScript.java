package proc;

import org.kie.api.runtime.process.ProcessContext;

import model.Request;

/**
 * MainScript
 */
public class MainScript {

    public static void dataValidation(ProcessContext kcontext) {
        Request request = (Request) kcontext.getVariable("request");
        boolean valid = request.getProcedure().length() != 0 && request.getProcessing().length() != 0;

        kcontext.setVariable("valid", valid);
    }
}